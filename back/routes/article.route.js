const router = require('express').Router();

const controller = require('../controllers/article.controller');

router.get('/', controller.get);
router.get('/:id', controller.getOne)
router.delete('/:id', controller.delete);

module.exports = router;
