const router = require('express').Router();

const articleRoute = require('./article.route');

router.use('/article', articleRoute);

module.exports = router;
