const Article = require("../models/article.model");

module.exports = {
  get: async function(req, res) {
    const {
      page: pageQuery = 1,
      limit: limitQuery = 10
    } = req.query;

    const limit = parseInt(limitQuery);
    const page = parseInt(pageQuery);

    const skip = parseInt(page * limit);
    const articles = await Article.find()
      .skip(skip)
      .limit(limit);

    return res.json(articles);
  },

  getOne: async function(req, res) {
    const { id: _id } = req.params;

    const article = await Article.findOne({ _id });
    if (!article)
      return res.status(404).json({ err: "Article not found" });

    return res.json(article);
  },

  delete: async function(req, res) {
    const { id: _id } = req.params;

    const deleted = await Article.deleteOne({ _id });
    if (deleted.n === 0)
      return res.status(404).json({ err: "Article not found" });

    return res.end();
  }
}
