const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ArticleSchema = new Schema({
	title: {
		type: String,
		required: true,
    trim: true
	},

  description: String,

  summary: String,

	date: {
    type: Date,
    required: true,
  },

  pubDate: {
    type: Date,
    required: true
  },

  link: {
    type: String,
    required: true,
    trim: true
  },

  author: String,

  categories: [String]
});

module.exports = mongoose.model('Article', ArticleSchema);
