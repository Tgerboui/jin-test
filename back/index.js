const expressConfig = require('./core/express.config');
const dbConfig = require('./core/mongo.config');
require('dotenv').config();

const initArticlesCollection = require('./init');

async function main() {
  await initArticlesCollection();
  dbConfig();
  const app = expressConfig();

  const ip = process.env.APP_HOST;
  const port = process.env.APP_PORT;

  const server = app.listen(port, ip, function() {
    console.log(`Server running on ${ip}:${port}`);
  });
}

main();
