const mongoose = require('mongoose');

module.exports = function() {
  const mongoOptions = {
    useUnifiedTopology: true,
    useNewUrlParser: true
  }

  const host = process.env.APP_DB_HOST;
  const port = process.env.APP_DB_PORT;
  const name = process.env.APP_DB_NAME;

  mongoose.connect(`mongodb://${host}:${port}/${name}`, mongoOptions);
}
