const express = require('express');
const helmet = require('helmet');
const cors = require('cors');
const bodyParser = require('body-parser');

const routes = require('../routes');

module.exports = function() {
  const app = express();

  //Parse body in JSON
  app.use(bodyParser.json());

  //Cors policy
  app.use(cors());
  app.options('*', cors());

  //Setup security with helmet
  app.use(helmet());

  //Setup routes
  app.use('/', routes);

  return app;
}
