const mongoose = require('mongoose');
const fs = require('fs');

const Article = require('./models/article.model');
const mongoConf = require('./core/mongo.config');

/*
This script will reset the article's collection
then push all the articles from json/ dir
*/

function getArticles(dirPath) {
  const articlesFiles = fs.readdirSync(dirPath);

  const articles = [];
  const articlesFilesLen = articlesFiles.length;
  for (let i = 0; i < articlesFilesLen; i++) {
    const file = articlesFiles[i];
    const articlesJSON = JSON.parse(fs.readFileSync(`${dirPath}/${file}`));
    articles.push(...articlesJSON);
  }

  return articles;
}

async function initArticlesCollection() {
  mongoConf();

  //Get articles
  const dirPath = `${__dirname}/json`;
  const articles = getArticles(dirPath);

  //Clean Article's collection
  try {
    await Article.deleteMany();
  }
  catch (err) {
    console.error("Error while deleting Article's collection :")
    throw err;
  }

  //Insert articles
  try {
    await Article.create(articles);
  }
  catch (err) {
    console.error("Error while inserting in Article's collection :")
    throw err;
  }

  console.log("Database reset and filled successfully !");

  mongoose.connection.close();
}

module.exports = initArticlesCollection;
