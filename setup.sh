#!/bin/bash


RED='\033[0;31m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
NC='\033[0m'

### Create variables and set in environment file ###

#Check if docker is installed
if ! hash docker 2> /dev/null; then
   echo "docker must be installed"
   exit;
fi

if ! hash docker-compose 2> /dev/null; then
   echo "docker-compose must be installed"
   exit;
fi

if [ -f back/.env ]; then
    rm -f back/.env
fi


#Register back env
touch back/.env
echo APP_PORT=3000 >> back/.env
echo APP_HOST=0.0.0.0 >> back/.env

echo APP_DB_HOST=db >> back/.env
echo APP_DB_PORT=27017 >> back/.env
echo APP_DB_NAME=jin >> back/.env


## Setup App ##
docker-compose up -d --build

echo -e "${GREEN}Web app generated${NC}"
echo ""
