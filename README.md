
# Jin Test

Jin test website.

This website parse json articles, records them in databse then show them on website.

## Getting started

### Install

Docker docker-compose and git are required for this project.

- Install git :  
```bash
sudo apt-get install git
```

- Install docker : [https://docs.docker.com/glossary/?term=install](https://docs.docker.com/glossary/?term=install)

- Install docker-compose : [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)

#### Setup

First clone the project :
```
git clone https://gitlab.com/Tgerboui/jin-test.git
```

Go to the project directory :
```
cd jin-test
```

Then launch the dockers :
```
./setup.sh
```

! Be carefull ! When the server start it reset always the database !

Then you can find the webiste on `localhost:8080`  
The back-end is host on `localhost:3000`

## Used in this project

* [MongoDB](https://www.mongodb.com/) - Database


* [Node.js](https://nodejs.org/) - Back-end javascript
* [Express](https://expressjs.com/) - Back-end framework


* [Vue.js](https://vuejs.org/) - Framework font-end

## Author

* **Thomas Gerbouin** - Développeur - thomas@gerbouin.com
